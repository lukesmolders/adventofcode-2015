﻿using System;
using System.Text;
using System.Threading;

namespace part4
{
    class Program
    {
        static void Main(string[] args)
        {
            var hash = "ckczppom";
            int i = 0;
            var outcome = "";

              while (true)
            {
                i++;
                outcome = CreateMD5($"{hash}{i}");
                if(i % 10000 == 0) {
                    Thread.Sleep(100);
                    Console.WriteLine($"we are at: {i}");
                }

                if (check(outcome))
                {
                    break;
                }
            } 

            Console.WriteLine("de uitkomst: " + outcome);
            Console.WriteLine("hash getal " + i); 
        }

        public static bool check(string input)
        {
            string first6 = input.Substring(0, 6);

            if (first6 == "000000")
            {
                return true;
            }

            return false;
        }

        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}
