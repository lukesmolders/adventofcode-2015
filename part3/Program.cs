﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace part3
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText("data.txt");

            // Position were at now
            var X = 0;
            var Y = 0;
            // nasty bool to fix the first visit
            var first = true;

            // Santa
            var santa = new Santa("santa");
            // Robo santa
            var robosanta = new Santa("robosanta");
            Santa santaSwitch;

            // List of houses 
            List<House> houses = new List<House>();

            var itsSint = false;

            foreach (char character in text)
            {
                itsSint = !itsSint; 
                switch (itsSint)
                {
                    case true:
                    santaSwitch = santa;
                    break;
                    case false:
                    santaSwitch = robosanta;
                    break;
                }

                switch (character)
                {
                    case '^':
                        santaSwitch.Y++;
                        Checker(santaSwitch, houses, first).Visits++;                    
                        break;
                    case '>':
                        santaSwitch.X++;
                        Checker(santaSwitch, houses, first).Visits++;
                        break;
                    case 'v':
                        santaSwitch.Y--;
                        Checker(santaSwitch, houses, first).Visits++;
                        break;
                    case '<':
                        santaSwitch.X--;
                        Checker(santaSwitch, houses, first).Visits++;
                        break;
                    default:
                        break;
                }
            }

            List<House> housesToCount = new List<House>();
            
             foreach (House house in houses.ToList())
             {
                 if(house.Visits >= 1) {
                    housesToCount.Add(house);
                 }
             }

            Console.WriteLine(housesToCount.Count);


        }


        public static House Checker(Santa santa, List<House> houses, bool first)
        {

            var row = from h in houses
                      where h.X == santa.X && h.Y == santa.Y
                      select h;


            var house1 = row.FirstOrDefault();
            var returningHouse = house1;

            if(house1 == null) {
                returningHouse = new House(santa.X,santa.Y);
                if(first) returningHouse.Visits++;
                houses.Add(returningHouse);
            }
 
            return returningHouse;
        }
    }

    internal class House
    {
        public int X;
        public int Y;
        public int Visits = 0;

        public House(int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }
    }

    internal class Santa
    {
        public int X = 0;
        public int Y = 0;

        public string name;

        public Santa(string name){
            this.name = name;
        }
    }

}
