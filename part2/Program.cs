﻿using System;
using System.Collections.Generic;

namespace part2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] text = System.IO.File.ReadAllLines("data.txt");
            var presenList = ConvertToPresentList(text);
            var totalWrappingPaperLength = CalculateWrappingPaper(presenList);
            var totalRibbon = CalculateWrappingLinter(presenList);

            Console.WriteLine($"wrapping paper : {totalWrappingPaperLength}");
            Console.WriteLine($"Ribbon : {totalRibbon}");

        }

        public static List<Present> ConvertToPresentList(string[] presents)
        {
            List<Present> presentList = new List<Present>();
            foreach (string presentString in presents)
            {
                var present = new Present();
                string[] presentArray = presentString.Split("x");
                present.length = Int32.Parse(presentArray[0]);
                present.width = Int32.Parse(presentArray[1]);
                present.height = Int32.Parse(presentArray[2]);

                presentList.Add(present);
            }

            return presentList;
        }

        public static int CalculateWrappingPaper(List<Present> presents)
        {
            var total = 0;
            foreach (var present in presents)
            {
                var lengthXwidth = 2 * (present.length * present.width);
                var widthXheight = 2 * (present.width * present.height);
                var heightXlength = 2 * (present.height * present.length);

                var smallestSide = Math.Min(lengthXwidth, Math.Min(widthXheight, heightXlength));
                var smallestside2 = smallestSide / 2;

                total += smallestside2 + lengthXwidth + widthXheight + heightXlength;
            }

            return total;
        }

        public static int CalculateWrappingLinter(List<Present> presents)
        {
            var total = 0;
            foreach (var present in presents)
            {
                var largestSde = Math.Max(present.length, Math.Max(present.width, present.height));
                var removeLargest = (present.length + present.width + present.height) - largestSde;
                var totalRibbon = removeLargest * 2;

                var ribbonCounter = (present.length * present.width) * present.height;

                total += (totalRibbon + ribbonCounter);
            }

            return total;
        }
    }

    class Present
    {
        public int length;
        public int width;
        public int height;
    }
}
