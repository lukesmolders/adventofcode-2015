﻿using System;

namespace adventofcode2015_part1
{
    class Program
    {
        static void Main(string[] args)
        {
            string text = System.IO.File.ReadAllText("input.txt");
            
            var floor = 0;
            var position = 0;
            foreach(char symbol in text) {
                position++;
                if(symbol.Equals('(')) {
                    floor++;
                }
                if(symbol.Equals(')')) {
                    floor--;
                }
                if(floor == -1) {
                    break;
                }

            }

            Console.WriteLine(floor);
            Console.WriteLine($"We zijn nu op positie: {position}");
        }
    }
}
